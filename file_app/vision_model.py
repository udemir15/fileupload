import numpy as np
import cv2
from tensorflow.keras.models import load_model

import os

model = load_model(os.path.join(os.path.dirname(__file__), "emojible_resnet.h5"))

class_names = np.array(['🍎', '👉', '🐤', '🍌', '🟦',
                        ',', '📝', '➗', '=', '👎',
                        '🙃', '🔁', '>', '🟩', '🤔',
                        '[', '💡', '➖', '✖️', '🔢',
                        '🦜', '📣', '➕', ']', '👻',
                        '🟥', '📱', '👋', '<', '🔉',
                        '💬', '🤚', '👍', '🪐'
                        ])


def extract_rois(image, filter_by_area=True, threshold_const=0.5):
    gray = cv2.cvtColor(image, cv2.COLOR_RGBA2GRAY)
    blur = cv2.medianBlur(gray, 5)
    sharpen_kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
    sharpen = cv2.filter2D(blur, -1, sharpen_kernel)

    thr = 50
    _, thresh = cv2.threshold(sharpen, thr, 255, cv2.THRESH_BINARY_INV)
    # plt.imshow(thresh,cmap='gray', vmin=0, vmax=255)

    cnts, hier = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    areas = np.array([cv2.contourArea(c) for c in cnts])
    median_area = np.median([area for area in areas if area > 10])
    if filter_by_area:
        areas = [area < (1 + threshold_const) * median_area
                 and area > (1 - threshold_const) * median_area
                 if area > 10 else False for area in areas]
    else:
        areas = [True for _ in range(len(areas))]

    coords = []

    for idx, roi_contour in enumerate(cnts):
        if areas[idx]:
            x, y, w, h = cv2.boundingRect(roi_contour)
            coords.append([x, y, w, h])

    return np.array(coords)


def extract_string_pos(coords, image, new_line_height_constant=0.7):
    mean_height = np.mean(coords[:, -1])
    y_sorted_args = np.argsort(coords[:, 1])
    y_sorted = coords[y_sorted_args]
    lines = [[y_sorted[0]]]
    for square in y_sorted[1:, :]:
        if square[1] > lines[-1][0][1] + mean_height * new_line_height_constant:
            lines.append([square])
        else:
            lines[-1].append(square)
    x_sorted_lines = []
    for line in lines:
        if len(line) == 1:
            x_sorted_lines.append(line)
        else:
            x_sorted_lines.append(sorted(line, key=lambda x: x[0]))
    return x_sorted_lines


def predict_emojis(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    low_dim = 1024
    if image.shape[1] < image.shape[0]:
        dim = (low_dim, int(image.shape[0] / image.shape[1] * low_dim))
    else:
        dim = (int(image.shape[1] / image.shape[0] * low_dim), low_dim)

    image = cv2.resize(image, dim)
    image = np.array(image)
    coords = extract_rois(image, filter_by_area=True)

    lines = extract_string_pos(coords, image)
    rois = np.array(
        [cv2.resize(image[roi[1]:roi[1] + roi[2], roi[0]:roi[0] + roi[3]], (64, 64)) / 255 for line in lines for roi in
         line])

    preds = model.predict(rois)
    pred_labels = np.argmax(preds, axis=1)
    pred_labels = class_names[pred_labels]

    st = ""
    i = 0
    for line in lines:
        for _ in range(len(line)):
            st += pred_labels[i]
            i += 1
        st += "\n"
    return st
