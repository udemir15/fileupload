from django.shortcuts import render

# Create your views here.

from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import status
from .serializers import FileSerializer
from .vision_model import predict_emojis
import os


class FileView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            im_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'media',
                                   file_serializer.data['file'].split('/')[-1])
            resp = {"code": predict_emojis(im_path)}
            return Response(resp, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
